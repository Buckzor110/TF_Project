package ru.tinkoff.tfpr.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;


public enum ApplicationError {
    STUDENT_ALREADY_EXISTS("Student already exists!", 400),
    COURSE_ALREADY_EXISTS("Course already exists!", 400),
    STUDENT_NOT_FOUND("Student not found", 404),
    COURSE_NOT_FOUND("Course not found", 404),
    INVALID_STUDENT_GRADE("Invalid student grade", 400),
    UNABLE_TO_CONNECT_DB("Unable to connect with database",500),
    UNABLE_TO_SEND_TO_TOPIC("Unable to send to topic using kafka", 500);
    private final String message;
    private final int code;

    ApplicationError(String message, int code) {
        this.message = message;
        this.code = code;
    }

    public ApplicationException exception(){
        return new ApplicationException(this);
    }

    public ApplicationException exception(String args){
        return new ApplicationException(this, args);
    }

    public static class ApplicationException extends RuntimeException{
        public final ApplicationExceptionCompanion applicationExceptionCompanion;

        ApplicationException(ApplicationError error){
            this.applicationExceptionCompanion = new ApplicationExceptionCompanion(error.code, error.message);
        }

        ApplicationException(ApplicationError error, String message){
            super(error.message+" : "+ message);
            this.applicationExceptionCompanion = new ApplicationExceptionCompanion(error.code, error.message+" : "+ message);
        }

        public static record ApplicationExceptionCompanion(@JsonIgnore int code, String message){

        }
    }




}
