package ru.tinkoff.tfpr.dao;
import org.apache.ibatis.annotations.Mapper;
import ru.tinkoff.tfpr.components.Course;

import java.util.ArrayList;

@Mapper
public interface CourseRepository {

    void savecourse(String id, String name, String description, int required_grade);

    void updateCourse(String id, String name, String description);

    ArrayList<Course> findAll();

    ArrayList<String> findStudents(String id);

    void deleteCourse(String uuid);

    Course findById(String id);

    Course TaskFind();
}
