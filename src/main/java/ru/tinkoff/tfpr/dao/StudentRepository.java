package ru.tinkoff.tfpr.dao;

import org.apache.ibatis.annotations.Mapper;
import ru.tinkoff.tfpr.components.Student;

import java.util.ArrayList;


@Mapper
public interface StudentRepository {

    void savestudent(Student student);

    ArrayList<Student> findAll();

    ArrayList<String> findCourses(String id);

    void updateStudent(String id, String name, int age);

    void deleteStudent(String uuid);

    Student findById(String id);

    void addToStudentsOutbox(String id, String messageId, String name, int age, int grade, String state, String isSent);

    String getMessageId();

    void updateMessageState(String messageId);

    Student getStudentFromStudentBox(String messageId);

    String getStateFromStudentBox(String messageId);

    void removeFromStudentOutbox();


}
