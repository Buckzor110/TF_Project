package ru.tinkoff.tfpr.dao;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ConnectionRepository {

    void addConnection(String course_id, String student_id);

    void deleteConnection(String course_id, String student_id);
}
