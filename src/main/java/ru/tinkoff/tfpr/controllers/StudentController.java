package ru.tinkoff.tfpr.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.tfpr.kafka.StudentProducer;
import ru.tinkoff.tfpr.components.Student;
import ru.tinkoff.tfpr.exception.ApplicationError;
import ru.tinkoff.tfpr.services.StudentService;

import javax.validation.Valid;

import static java.lang.String.format;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static ru.tinkoff.tfpr.exception.ApplicationError.STUDENT_ALREADY_EXISTS;
import static ru.tinkoff.tfpr.exception.ApplicationError.STUDENT_NOT_FOUND;

@RestController
@RequestMapping(path = "/students/")
@Slf4j
public class StudentController {

    @Autowired
    private StudentService studentService;


    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public void  addStudent(@RequestBody @Valid Student student){
        try{ studentService.save(student);
        } catch (Exception e){
            throw STUDENT_ALREADY_EXISTS.exception(format("Student name = %s",student.getName() ));
        }
        log.info("New student with name {} has been added!", student.getName());
    }
    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public Student getStudent(@RequestParam("id") String id){
       try{return studentService.findStudent(id);
    } catch (Exception e){
        throw STUDENT_NOT_FOUND.exception(id);
    }
    }

    @DeleteMapping
    public void deleteStudent(@RequestParam("id") String id){
        try{studentService.deleteStudent(id);
            log.info("Student with id {} was deleted!", id);
    } catch (Exception e){
        throw STUDENT_NOT_FOUND.exception(id);
    }
    }

    @PatchMapping(consumes = APPLICATION_JSON_VALUE)
    public void updateStudent(@RequestBody Student student){
        try{studentService.updateStudent(student.getId(), student.getName(), student.getAge());
            log.info("Student with name {} was updated!", student.getName());
        } catch (Exception e){
        throw STUDENT_NOT_FOUND.exception(student.getId());
    }
    }

    @ExceptionHandler(ApplicationError.ApplicationException.class)
    public ResponseEntity<ApplicationError.ApplicationException.ApplicationExceptionCompanion> handleApplicationException(ApplicationError.ApplicationException e){
        return ResponseEntity.status(e.applicationExceptionCompanion.code()).body(e.applicationExceptionCompanion);
    }


}
