package ru.tinkoff.tfpr.controllers;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.tinkoff.tfpr.components.Course;
import ru.tinkoff.tfpr.components.Student;
import ru.tinkoff.tfpr.exception.ApplicationError;
import ru.tinkoff.tfpr.services.CourseService;
import ru.tinkoff.tfpr.services.StudentService;

import javax.validation.Valid;

import static java.lang.String.format;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static ru.tinkoff.tfpr.exception.ApplicationError.*;

@RestController
@RequestMapping(path = "/courses/")
@Slf4j
public class CourseController {

    private final StudentService studentService;
    private final CourseService courseService;

    public CourseController(StudentService studentService, CourseService courseService) {
        this.studentService = studentService;
        this.courseService = courseService;
    }

    @PostMapping
    public void addStudents(@RequestParam("course_id") String course_id, @RequestParam("student_id") String student_id){
        try{studentService.findStudent(student_id);}
        catch(Exception e){
            throw STUDENT_NOT_FOUND.exception(student_id);
        }

        try{courseService.findCourse(course_id);}
        catch(Exception e){
            throw COURSE_NOT_FOUND.exception(student_id);
        }

        Student student = studentService.findStudent(student_id);
        Course course = courseService.findCourse(course_id);

        if (student.getGrade()>=course.getRequired_grade()) {
            courseService.addStudent(course, student);
            log.info("Student with name {} has been added to course with name {}!", student.getName(), course.getName());
        }
        else{
            log.info("Student grade is not valid!");
        }
    }


    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public void addCourse(@RequestBody @Valid Course course){
        try{ courseService.save(course);
        } catch (Exception e){
        throw COURSE_ALREADY_EXISTS.exception(format("Board game name = %s",course.getName() ));
        }
        log.info("New course with name {} has been added!", course.getName());
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public Course getCourse(@RequestParam("id") String id){
        try {return courseService.findCourse(id);
        } catch (Exception e){
        throw COURSE_NOT_FOUND.exception(id);
    }
    }

    @DeleteMapping
    public void deleteCourse(@RequestParam("id") String id){
        try{courseService.deleteCourse(id);
    } catch (Exception e){
        throw COURSE_NOT_FOUND.exception(id);
    }
    }

    @PatchMapping(consumes = APPLICATION_JSON_VALUE)
    public void updateCourse(@RequestBody Course course){
        try{courseService.updateCourse(course.getId(), course.getName(), course.getDescription());
    } catch (Exception e){
        throw COURSE_NOT_FOUND.exception(course.getId());
    }
    }
    @ExceptionHandler(ApplicationError.ApplicationException.class)
    public ResponseEntity<ApplicationError.ApplicationException.ApplicationExceptionCompanion> handleApplicationException(ApplicationError.ApplicationException e){
        return ResponseEntity.status(e.applicationExceptionCompanion.code()).body(e.applicationExceptionCompanion);
    }

}

