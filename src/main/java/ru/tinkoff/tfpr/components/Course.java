package ru.tinkoff.tfpr.components;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import ru.tinkoff.tfpr.validators.CourseConstraint;

import java.util.List;

@Data
@NoArgsConstructor
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@CourseConstraint
public class Course{

    @NonNull String id ;
    @NonNull String name;
    @NonNull String description;
    List<String> students;
    @NonNull int required_grade;



}
