package ru.tinkoff.tfpr.components;

import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import ru.tinkoff.tfpr.validators.StudentConstraint;

import java.util.List;


@Data
@RequiredArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
@StudentConstraint
public class Student {

    @NonNull String id;
    @NonNull String name;
    @NonNull int age;
    @NonNull List<String> courses;
    @NonNull int grade;

    public Student(String id, String name, int age){
        this.id = id;
        this.name = name;
        this.age = age;
    }





}
