package ru.tinkoff.tfpr.cache;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import ru.tinkoff.tfpr.components.Course;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Slf4j
@Component
public class CourseCache {

    private final Map<String, Course> cache = new ConcurrentHashMap<>();

    public void addCourse(Course course){
        cache.put(course.getId(), course);
        log.info("Cache successfully added");
    }

    public Course findCourse(String id){
        return cache.get(id);
    }

    public Map<String, Course> getCache() {
        return cache;
    }

    public void removeCourse(String id){
        this.cache.remove(id);
        log.info("Cache successfully removed");
    }
}