package ru.tinkoff.tfpr.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;



@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfigurer extends WebSecurityConfigurerAdapter {


    @Autowired
    public void configureAdmin(AuthenticationManagerBuilder auth, PasswordEncoder encoder, @Value("${admin.name}") String login, @Value("${admin.password}") String password) throws Exception{
        auth.inMemoryAuthentication()
                .withUser(login).password(encoder.encode(password))
                .roles("USER","ADMIN");
    }

    @Autowired
    public void configureUser(AuthenticationManagerBuilder auth, PasswordEncoder encoder, @Value("user.name") String login, @Value("user.password") String password) throws Exception{
        auth.inMemoryAuthentication().withUser(login).password(encoder.encode(password))
                .roles("USER");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable().authorizeRequests()
                .antMatchers(HttpMethod.POST,"/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.DELETE, "/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.PATCH, "/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.GET, "/**").hasRole("USER")
                .anyRequest().authenticated()
                .and()
                .httpBasic();
    }
}
