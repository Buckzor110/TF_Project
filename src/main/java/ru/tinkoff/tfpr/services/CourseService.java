package ru.tinkoff.tfpr.services;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tinkoff.tfpr.cache.CourseCache;
import ru.tinkoff.tfpr.components.Course;
import ru.tinkoff.tfpr.components.Student;
import ru.tinkoff.tfpr.dao.ConnectionRepository;
import ru.tinkoff.tfpr.dao.CourseRepository;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;
import static java.util.Objects.nonNull;

@Service
@Slf4j
public class CourseService {

    @Autowired
    private CourseCache courseCache;

    @Autowired
    private ConnectionRepository connectionRepository;

    @Autowired
    private CourseRepository repository;

    public synchronized void save(Course course) {
        repository.savecourse(course.getId(), course.getName(), course.getDescription(), course.getRequired_grade());
        courseCache.addCourse(course);
    }

    public synchronized void addStudent(Course course, Student student){
        connectionRepository.addConnection(course.getId(), student.getId());
        courseCache.removeCourse(course.getId());
    }


    public synchronized void updateCourse(String id, String name, String description) throws Exception {
        if (nonNull(repository.findById(id))){
            repository.updateCourse(id, name, description);
            courseCache.removeCourse(id);
        }
        else{
            throw new Exception(format("Курс с id %s не найден!", id));
        }
    }

    public synchronized void deleteCourse(String id) throws Exception{
        if (nonNull(repository.findById(id))) {
            repository.deleteCourse(id);
            courseCache.removeCourse(id);
        }
        else{
            throw new Exception(format("Курс с id %s не найден!", id));
        }
    }

    public Course TaskFind() {return repository.TaskFind();}

    public Course findCourse(String id) {
        return courseCache.getCache().computeIfAbsent(id, k ->{
            Course course = repository.findById(id);
            course.setStudents(findStudents(course.getId()));
            return course;});
    }


    public List<String> findStudents(String id){
        if (repository.findStudents(id).isEmpty())
        {
            return null;
        } else{
            return repository.findStudents(id);

        }
    }

    public ArrayList<Course> findAll() {
        return repository.findAll();
    }
}
