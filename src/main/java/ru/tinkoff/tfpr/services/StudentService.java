package ru.tinkoff.tfpr.services;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ExceptionHandler;
import ru.tinkoff.tfpr.components.Course;
import ru.tinkoff.tfpr.components.Student;
import ru.tinkoff.tfpr.dao.ConnectionRepository;
import ru.tinkoff.tfpr.dao.StudentRepository;
import ru.tinkoff.tfpr.exception.ApplicationError;

import java.util.List;
import java.util.UUID;

import static java.util.Objects.nonNull;
import static ru.tinkoff.tfpr.exception.ApplicationError.UNABLE_TO_CONNECT_DB;


@Service
@Transactional
public class StudentService {

    private final ConnectionRepository connectionRepository;

    private final StudentRepository repository;

    public StudentService(StudentRepository repository, ConnectionRepository connectionrepository) {
        this.repository = repository;
        this.connectionRepository = connectionrepository;
    }


    public synchronized void save(Student student) {
        try {
            repository.savestudent(student);
            repository.addToStudentsOutbox(student.getId(), UUID.randomUUID().toString(), student.getName(), student.getAge(), student.getGrade(), "saved", "false");
            for (String course : student.getCourses()) {
                connectionRepository.addConnection(course, student.getId());
            }
        } catch (Exception e){
            throw UNABLE_TO_CONNECT_DB.exception();
        }
    }

    public Student findStudent(String id) {
        Student student = repository.findById(id);
        student.setCourses(findCourses(student.getId()));
        return student;
    }


    public synchronized void deleteStudent(String id){
        try{
            if (nonNull(repository.findById(id))) {
                Student student = repository.findById(id);
                repository.deleteStudent(id);
                repository.addToStudentsOutbox(student.getId(), UUID.randomUUID().toString(), student.getName(), student.getAge(), student.getGrade(), "deleted", "false");
            }
        }
        catch(Exception e){
            throw UNABLE_TO_CONNECT_DB.exception(id);
        }
    }


    public synchronized void updateStudent(String id, String name, int age) {
        try{
            if (nonNull(repository.findById(id))) {
                repository.updateStudent(id, name, age);
                Student student = repository.findById(id);
                repository.addToStudentsOutbox(student.getId(), UUID.randomUUID().toString(), student.getName(), student.getAge(), student.getGrade(), "updated", "false");

            }
        }
        catch(Exception e){
            throw UNABLE_TO_CONNECT_DB.exception(id);
        }
   }




    public void addCourse(Student student, Course course){
       connectionRepository.addConnection(course.getId(), student.getId());
    }

    public void deleteCourse(Student student, Course course){
        connectionRepository.deleteConnection(course.getId(), student.getId());
    }

    public List<Student> findAll() {
       List<Student> all = repository.findAll();
       for (Student student: all){
           student.setCourses(findCourses(student.getId()));
       }
       return all;
   }

    public List<String> findCourses(String id){
        return repository.findCourses(id);
    }

    public Student getStudentFromStudentBox(String id){
        return repository.getStudentFromStudentBox(id);
    }

    public String getMessageId(){
        return repository.getMessageId();
    }

    public void updateMessageState(String id){
        repository.updateMessageState(id);
    }

    public String getStateFromStudentBox(String id){
        return repository.getStateFromStudentBox(id);
    }

    public void removeFromStudentOutbox(){
        repository.removeFromStudentOutbox();
    }

    @ExceptionHandler(ApplicationError.ApplicationException.class)
    public ResponseEntity<ApplicationError.ApplicationException.ApplicationExceptionCompanion> handleApplicationException(ApplicationError.ApplicationException e){
        return ResponseEntity.status(e.applicationExceptionCompanion.code()).body(e.applicationExceptionCompanion);
    }
}
