package ru.tinkoff.tfpr.kafka;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.tinkoff.tfpr.services.StudentService;

import static ru.tinkoff.tfpr.exception.ApplicationError.UNABLE_TO_SEND_TO_TOPIC;


@Service
@EnableScheduling
public class StudentProducer {

    @Autowired
    private StudentService studentService;

    @Autowired
    KafkaTemplate<String, String> kafkaTemplate;

    @Value("${spring.kafka.topic.name}")
    private String topic;

    @Scheduled(fixedRate = 1000L)
    public void produce(){
        if (studentService.getMessageId()!=null){
            String messageId = studentService.getMessageId();
            String data = studentService.getStudentFromStudentBox(messageId).toString() + " - " + studentService.getStateFromStudentBox(messageId);
            System.out.println(data);
            try {
                kafkaTemplate.send(topic, data);
            }
            catch (Exception e){
                throw UNABLE_TO_SEND_TO_TOPIC.exception();
            }
            studentService.updateMessageState(messageId);
        }

    }

}
