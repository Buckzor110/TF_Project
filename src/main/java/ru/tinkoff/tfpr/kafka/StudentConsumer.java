package ru.tinkoff.tfpr.kafka;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class StudentConsumer {

        @KafkaListener(topics = "${spring.kafka.topic.name}")
        public void consume(String message){
            System.out.println("Consuming the message: " + message);

    }
}
