package ru.tinkoff.tfpr.validators;

import ru.tinkoff.tfpr.components.Course;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CourseValidator implements ConstraintValidator<CourseConstraint, Course> {

    @Override
    public boolean isValid(Course course, ConstraintValidatorContext constraintValidatorContext) {
        if (course.getName() == null || course.getId() == null || course.getId().isBlank() || course.getName().isBlank()){
            return false;
        }
        else return !(course.getDescription() == null || course.getDescription().isBlank());
    }
}
