package ru.tinkoff.tfpr.validators;

import ru.tinkoff.tfpr.components.Student;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class StudentValidator implements ConstraintValidator<StudentConstraint, Student> {

    @Override
    public boolean isValid(Student student, ConstraintValidatorContext constraintValidatorContext) {
        if (student.getName() == null || student.getId() == null || student.getId().isBlank() || student.getName().isBlank()) {
            return false;
        } else return (student.getAge() >= 14);
    }
}
