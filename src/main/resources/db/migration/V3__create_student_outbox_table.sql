CREATE TABLE students_outbox
(
    messageId   VARCHAR(100) PRIMARY KEY,
    id   VARCHAR(100),
    name VARCHAR(100) ,
    age  INT,
    grade INT,
    state VARCHAR(100),
    isSent VARCHAR(100)
);