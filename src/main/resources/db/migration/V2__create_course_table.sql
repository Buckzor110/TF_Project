CREATE TABLE courses
(
    id VARCHAR(100) PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    description VARCHAR(100) NOT NULL,
    required_grade INT
);

CREATE TABLE Students_To_Course
(
    student_id VARCHAR(100) NOT NULL, FOREIGN KEY (student_id) REFERENCES students(id) ON DELETE CASCADE,
    course_id VARCHAR(100) NOT NULL, FOREIGN KEY (course_id) REFERENCES courses(id) ON DELETE CASCADE
);