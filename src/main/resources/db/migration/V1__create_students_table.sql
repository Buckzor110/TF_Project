CREATE TABLE students
(
    id   VARCHAR(100) PRIMARY KEY,
    name VARCHAR(100) NOT NULL,
    age  INT         NOT NULL,
    grade INT
);