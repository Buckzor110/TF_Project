package ru.tinkoff.tfpr.validators;

import org.junit.jupiter.api.Test;
import ru.tinkoff.tfpr.components.Course;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;


class CourseValidatorTest {

    private final CourseValidator courseValidator = new CourseValidator();

    @Test
    void testValid() {
        assertTrue(courseValidator.isValid(prepareValidCourse(), null));
    }

    @Test
    void testInvalidName() {
        assertFalse(courseValidator.isValid(prepareInvalidNameCourse(), null));
    }

    @Test
    void testInvalidId() {
        assertFalse(courseValidator.isValid(prepareInvalidIdCourse(), null));
    }

    @Test
    void testInvalidDescription() {
        assertFalse(courseValidator.isValid(prepareInvalidDescriptionCourse(), null));
    }

    private Course prepareValidCourse(){
        return new Course(UUID.randomUUID().toString(), "test", "Cool Course", 21);
    }

    private Course prepareInvalidNameCourse(){
        return new Course("72854426-1223-42c5-8a7d-fa22369f70ab", "", "Cool Course", 21);
    }

    private Course prepareInvalidIdCourse(){
        return new Course("", "test", "Cool Course", 21);
    }

    private Course prepareInvalidDescriptionCourse(){
        return new Course("72854426-1223-42c5-8a7d-fa22369f70ab", "test", "", 21);
    }

}