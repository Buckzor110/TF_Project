package ru.tinkoff.tfpr.validators;

import org.junit.jupiter.api.Test;
import ru.tinkoff.tfpr.components.Student;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class StudentValidatorTest {

    private final StudentValidator studentValidator = new StudentValidator();
    @Test
    void testValid() {
        assertTrue(studentValidator.isValid(prepareValidStudent(), null));
    }

    @Test
    void testInvalidName() {
        assertFalse(studentValidator.isValid(prepareInvalidNameStudent(), null));
    }

    @Test
    void testInvalidId() {
        assertFalse(studentValidator.isValid(prepareInvalidIdStudent(), null));
    }

    @Test
    void testInvalidDescription() {
        assertFalse(studentValidator.isValid(prepareInvalidAgeStudent(), null));
    }

    private Student prepareValidStudent(){
        return new Student(UUID.randomUUID().toString(), "test", 16);
    }

    private Student prepareInvalidNameStudent(){
        return new Student("72854426-1223-42c5-8a7d-fa22369f70ab", "", 15);
    }

    private Student prepareInvalidIdStudent(){
        return new Student("", "test", 14);
    }

    private Student prepareInvalidAgeStudent(){
        return new Student("72854426-1223-42c5-8a7d-fa22369f70ab", "test", 7);
    }

}