package ru.tinkoff.tfpr.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ru.tinkoff.tfpr.AbstractClass;
import ru.tinkoff.tfpr.components.Student;
import ru.tinkoff.tfpr.services.StudentService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WithMockUser(username = "logadm", password = "testadm", roles = {"USER","ADMIN"})
class StudentControllerTest extends AbstractClass {

    @Autowired
    private StudentService studentService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper jackson;

    @Before
    public void setUp() throws Exception{
        mockMvc.perform(post("/courses/").contentType("application/json").content(jackson.writeValueAsString(fintech)));
        mockMvc.perform(post("/courses/").contentType("application/json").content(jackson.writeValueAsString(cooler_fintech)));
    }
    @Test
    @WithAnonymousUser
    void authenticationFail() throws Exception{
        var studentJson = jackson.writeValueAsString(john);
        mockMvc.perform(post("/students/").contentType("application/json").content(studentJson)).andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "any", password = "some", roles = "USER")
    void authorizationFail() throws Exception{
        var studentJson = jackson.writeValueAsString(john);
        mockMvc.perform(post("/students/").contentType("application/json").content(studentJson)).andExpect(status().isForbidden());
    }

    @Test
    void studentNotFoundTest() throws Exception{
        mockMvc.perform(get("/students/").param("id", john.getId())).andExpect(status().is4xxClientError());
    }

    @Test
    void studentFound() throws Exception{
        mockMvc.perform(post("/students/").contentType("application/json").content(jackson.writeValueAsString(john)));
        var result = mockMvc.perform(get("/students/").param("id", john.getId())).andReturn().getResponse().getContentAsString();
        var check = studentService.findStudent(john.getId());
        assertEquals(check, jackson.readValue(result, Student.class));
    }

    @Test
    void successfulDelete() throws Exception{
        var courseJson = jackson.writeValueAsString(john);
        mockMvc.perform(post("/students/").contentType("application/json").content(courseJson));
        mockMvc.perform(delete("/students/").param("id", john.getId())).andExpect(status().isOk());
    }

    @Test
    void deleteNotFound() throws Exception{
        mockMvc.perform(delete("/students/").param("id", john.getId())).andExpect(status().is4xxClientError());
    }

    @Test
    void addInvalidNameStudent()throws Exception{
        var course = prepareInvalidNameStudent();
        var courseJson = jackson.writeValueAsString(course);
        mockMvc.perform(post("/courses/").contentType("application/json").content(courseJson)).andExpect(status().isBadRequest());
    }


    @Test
    void addInvalidAgeStudent()throws Exception{
        var course = prepareInvalidAgeStudent();
        var courseJson = jackson.writeValueAsString(course);
        mockMvc.perform(post("/students/").contentType("application/json").content(courseJson)).andExpect(status().isBadRequest());
    }

    @Test
    void addInvalidIdStudent()throws Exception{
        var course = prepareInvalidIdStudent();
        var courseJson = jackson.writeValueAsString(course);
        mockMvc.perform(post("/students/").contentType("application/json").content(courseJson)).andExpect(status().isBadRequest());
    }

    @Test
    void studentsAlreadyExists() throws Exception{
        var courseJson = jackson.writeValueAsString(john);
        mockMvc.perform(post("/students/").contentType("application/json").content(courseJson));
        mockMvc.perform(post("/students/").contentType("application/json").content(courseJson)).andExpect(status().is4xxClientError());
    }

    @Test
    void patchSuccess()throws Exception{
        var courseJson = jackson.writeValueAsString(john);
        var updatedJohn = patchStudent();
        var updatedJohnJson = jackson.writeValueAsString(updatedJohn);
        mockMvc.perform(post("/students/").contentType("application/json").content(courseJson));
        mockMvc.perform(patch("/students/").contentType("application/json").content(updatedJohnJson)).andExpect(status().isOk());

    }

    @Test
    void patchFail()throws Exception{
        var updatedJohn = patchStudent();
        var updatedJohnJson = jackson.writeValueAsString(updatedJohn);
        mockMvc.perform(patch("/students/").contentType("application/json").content(updatedJohnJson)).andExpect(status().is4xxClientError());

    }


    private Student prepareInvalidNameStudent(){
        return new Student("72854426-1223-42c5-8a7d-fa22369f70ab", "", 17, existcourses, 15);
    }

    private Student prepareInvalidIdStudent(){
        return new Student("", "test", 20, existcourses, 15);
    }

    private Student prepareInvalidAgeStudent(){
        return new Student("72854426-1223-42c5-8a7d-fa22369f70ab", "test", 10, existcourses, 15);
    }

    private Student patchStudent(){
        return new Student(john.getId(), "Updated test", 19, existcourses, 15);
    }








}