package ru.tinkoff.tfpr.controllers;


import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.test.context.support.WithAnonymousUser;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.WebApplicationContext;
import ru.tinkoff.tfpr.AbstractClass;
import ru.tinkoff.tfpr.components.Course;
import ru.tinkoff.tfpr.services.CourseService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@WithMockUser(username = "any", password = "thing", roles = {"USER","ADMIN"})
class CourseControllerTest extends AbstractClass {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private CourseService courseService;

    @Autowired
    private ObjectMapper jackson;

    @Autowired
    private WebApplicationContext context;

    @Test
    @Order(1)
    void courseNotFoundTest() throws Exception{
        var course = prepareValidCourse().getId();
        mockMvc.perform(get("/courses/").param("id", course)).andExpect(status().is4xxClientError());
    }

    @Test
    @WithAnonymousUser
    void authenticationFail() throws Exception{
        var course = prepareValidCourse();
        var courseJson = jackson.writeValueAsString(course);
        mockMvc.perform(post("/courses/").contentType("application/json").content(courseJson)).andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser(username = "any", password = "some", roles = "USER")
    void authorizationFail() throws Exception{
        var course = prepareValidCourse();
        var courseJson = jackson.writeValueAsString(course);
        mockMvc.perform(post("/courses/").contentType("application/json").content(courseJson)).andExpect(status().isForbidden());
    }

    @Test
    void addValidCourse() throws Exception{
        var course = prepareValidCourse();
        var courseJson = jackson.writeValueAsString(course);
        mockMvc.perform(post("/courses/").contentType("application/json").content(courseJson)).andExpect(status().isOk());
        var check = courseService.findCourse(course.getId());
        assertEquals(course, check);
    }

    @Test
    void courseFound() throws Exception{
        var course = prepareValidCourse();
        var courseJson = jackson.writeValueAsString(course);
        mockMvc.perform(post("/courses/").contentType("application/json").content(courseJson));
        var check1 = mockMvc.perform(get("/courses/").param("id", course.getId()));
        var check = courseService.findCourse(course.getId());
        assertEquals(check, jackson.readValue(check1.andReturn().getResponse().getContentAsString(), Course.class));
    }

    @Test
    void successfulDelete() throws Exception{
        var course = prepareValidCourse();
        var courseJson = jackson.writeValueAsString(course);
        mockMvc.perform(post("/courses/").contentType("application/json").content(courseJson));
        mockMvc.perform(delete("/courses/").param("id", course.getId())).andExpect(status().isOk());
    }

    @Test
    void deleteNotFound() throws Exception{
        var course = prepareValidCourse();
        mockMvc.perform(delete("/courses/").param("id", course.getId())).andExpect(status().is4xxClientError());
    }

    @Test
    void successfulUpdate() throws Exception{
        var course = prepareValidCourse();
        var updatedCourse = prepareUpdatedCourse();
        var courseJson = jackson.writeValueAsString(course);
        var updatedCourseJson = jackson.writeValueAsString(updatedCourse);
        mockMvc.perform(post("/courses/").contentType("application/json").content(courseJson));
        mockMvc.perform(patch("/courses/").contentType("application/json").content(updatedCourseJson)).andExpect(status().isOk());
    }

    @Test
    void updateNotFound() throws Exception{
        var updatedCourse = prepareUpdatedCourse();
        var updatedCourseJson = jackson.writeValueAsString(updatedCourse);
        mockMvc.perform(patch("/courses/").contentType("application/json").content(updatedCourseJson)).andExpect(status().is4xxClientError());
    }


    @Test
    void successfulAdd() throws Exception{
        var fintechJson = jackson.writeValueAsString(fintech);
        var coolerFintechJson = jackson.writeValueAsString(cooler_fintech);

        mockMvc.perform(post("/courses/").contentType("application/json").content(fintechJson));
        mockMvc.perform(post("/courses/").contentType("application/json").content(coolerFintechJson));

        var studentJson = jackson.writeValueAsString(john);
        mockMvc.perform(post("/students/").contentType("application/json").content(studentJson));

        var course = prepareValidCourse();
        var courseJson = jackson.writeValueAsString(course);
        mockMvc.perform(post("/courses/").contentType("application/json").content(courseJson));

        mockMvc.perform(post("/courses/").queryParam("course_id", course.getId()).queryParam("student_id", john.getId())).andExpect(status().isOk());
    }

    @Test
    void studentNotFound() throws Exception{
        var course = prepareValidCourse();
        var courseJson = jackson.writeValueAsString(course);
        mockMvc.perform(post("/courses/").contentType("application/json").content(courseJson));
        mockMvc.perform(post("/courses/").queryParam("course_id", course.getId()).queryParam("student_id", john.getId())).andExpect(status().is4xxClientError());
    }

    @Test
    @Order(2)
    void courseNotFound() throws Exception{

        mockMvc.perform(post("/courses/").contentType("application/json").content(jackson.writeValueAsString(fintech)));
        mockMvc.perform(post("/courses/").contentType("application/json").content(jackson.writeValueAsString(cooler_fintech)));

        var johnJson = jackson.writeValueAsString(john);
        mockMvc.perform(post("/students/").contentType("application/json").content(johnJson));
        mockMvc.perform(post("/courses/").queryParam("course_id", prepareValidCourse().getId()).queryParam("student_id", john.getId())).andExpect(status().is4xxClientError());
    }

    @Test
    void addInvalidNameCourse()throws Exception{
        var course = prepareInvalidNameCourse();
        var courseJson = jackson.writeValueAsString(course);
        mockMvc.perform(post("/courses/").contentType("application/json").content(courseJson)).andExpect(status().isBadRequest());
    }


    @Test
    void addInvalidDescriptionCourse()throws Exception{
        var course = prepareInvalidDescriptionCourse();
        var courseJson = jackson.writeValueAsString(course);
        mockMvc.perform(post("/courses/").contentType("application/json").content(courseJson)).andExpect(status().isBadRequest());
    }

    @Test
    void addInvalidIdCourse()throws Exception{
        var course = prepareInvalidIdCourse();
        var courseJson = jackson.writeValueAsString(course);
        mockMvc.perform(post("/courses/").contentType("application/json").content(courseJson)).andExpect(status().isBadRequest());
    }

    @Test
    void CourseAlreadyExists() throws Exception{
        var course = prepareValidCourse();
        var courseJson = jackson.writeValueAsString(course);
        mockMvc.perform(post("/courses/").contentType("application/json").content(courseJson));
        mockMvc.perform(post("/courses/").contentType("application/json").content(courseJson)).andExpect(status().is4xxClientError());
    }



    private Course prepareUpdatedCourse(){
        return new Course("72853426-1223-42c5-8a7d-fa22369f70ab", "Updated test", "Cool Course", 21);
    }

    private Course prepareValidCourse(){
        return new Course("72853426-1223-42c5-8a7d-fa22369f70ab", "test", "Cool Course", 21);
    }

    private Course prepareInvalidNameCourse(){
        return new Course("72854426-1223-42c5-8a7d-fa22369f70ab", "", "Cool Course", 21);
    }

    private Course prepareInvalidIdCourse(){
        return new Course("", "test", "Cool Course", 21);
    }

    private Course prepareInvalidDescriptionCourse(){
        return new Course("72854426-1223-42c5-8a7d-fa22369f70ab", "test", "",21);
    }


}