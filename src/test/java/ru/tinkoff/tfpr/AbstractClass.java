package ru.tinkoff.tfpr;


import org.junit.jupiter.api.BeforeAll;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.springframework.transaction.annotation.Transactional;
import org.testcontainers.containers.MySQLContainer;
import ru.tinkoff.tfpr.components.Course;
import ru.tinkoff.tfpr.components.Student;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@SpringBootTest
@AutoConfigureMockMvc
@Transactional
@ContextConfiguration(initializers = AbstractClass.class)
public class AbstractClass implements ApplicationContextInitializer<ConfigurableApplicationContext> {


    static public Course fintech = new Course(UUID.randomUUID().toString(),"Fintech", "Cool Course", 20);
    static public Course cooler_fintech = new Course(UUID.randomUUID().toString(),"Still the same Fintech", "But cooler", 20);
    static public Course english = new Course(UUID.randomUUID().toString(),"English courses", "Is nice", 20 );

    static public List<String> existcourses = new ArrayList<>(List.of(fintech.getId(), cooler_fintech.getId()));
    static public List<String> morecourses = new ArrayList<>(List.of(english.getId(), cooler_fintech.getId()));


    static public Student john = new Student(UUID.randomUUID().toString(),"John", 27, existcourses, 23);

    private static final MySQLContainer<?> mysql = new MySQLContainer<>("mysql:latest").withReuse(true);

    @BeforeAll
    public static void setup(){
        mysql.start();
    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {

        TestPropertySourceUtils.addInlinedPropertiesToEnvironment(
                applicationContext,
                "spring.datasource.url ="+ mysql.getJdbcUrl(),
                "spring.datasource.username ="+ mysql.getUsername(),
                "spring.datasource.password ="+ mysql.getPassword()
        );
    }
}
