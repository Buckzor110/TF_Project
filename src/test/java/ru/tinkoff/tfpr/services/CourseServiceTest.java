package ru.tinkoff.tfpr.services;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.tinkoff.tfpr.AbstractClass;
import ru.tinkoff.tfpr.cache.CourseCache;
import ru.tinkoff.tfpr.components.Course;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

class CourseServiceTest extends AbstractClass {

    @Autowired
    private CourseService courseService;

    @Autowired
    private CourseCache courseCache;

    @Test
    public void getCourseFromTableToCache(){
        Course test = prepareCourse();
        courseService.save(test);
        courseCache.removeCourse(test.getId());
        assertNull(courseCache.findCourse(test.getId()));
        courseService.findCourse(test.getId());
        assertEquals(test, courseCache.findCourse(test.getId()));
    }

    @Test
    public void deleteCourse() throws Exception {
        Course test = prepareCourse();
        courseService.save(test);
        assertNotNull(courseCache.findCourse(test.getId()));
        courseService.deleteCourse(test.getId());
        assertNull(courseCache.findCourse(test.getId()));
    }

    @Test
    public void createCourse(){
        Course test = prepareCourse();
        assertNull(courseCache.findCourse(test.getId()));
        courseService.save(test);
        assertEquals(test, courseCache.findCourse(test.getId()));
    }

    @Test
    public void getCourseFromCache(){
        Course test = prepareCourse();
        courseService.save(test);
        courseService.findCourse(test.getId());
    }

    private Course prepareCourse(){
        return new Course(UUID.randomUUID().toString(), "Test One", "Sample Description", 17);
    }
}